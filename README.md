Small module to provide a context stack to logging messages.

Given a situation like:

```python

logger = logging.getLogger( __name__ )
handler = ...
handler.setFormatter( ctxlogger.ContextFormatter() )
logger.setHandler( handler )

def get_orders( config ):

	for server in config:
		with ctxlogger.context( 'server', server ):
			get_orders_from_server( server )

def get_orders_from_server( server ):

	for file in get_files_on_server( server ):
		with ctxlogger.context( 'file', file ):
			get_orders_from_file( file )

def get_orders_from_file( file ):

	for order in parse_file( file ):
		with ctxlogger.context( 'order', order ):
			if not validate_order( order ):
				logger.error( 'Order failed validation' )
```

a failing order would log:

`timestamp: 2015-08-14 14:54:30,745 // level: ERROR // server: sftp.madeupclient.com // file: orders.csv // order: 12345-678 // Order failed validation`
